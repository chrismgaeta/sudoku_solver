# Sudoku Solver #

Naive sudoku web app that runs on Angular.js. Uses constraints to solve as many squares as possible.

Created as an experiment with Angular modules, Services, and data-binding.